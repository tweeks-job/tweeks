package ca.tweeks.service;

import org.springframework.stereotype.Service;

@Service
public class GreetingService {

    private String testString;

    public String showMessage()
    {
        return testString;
    }
    public void setTestString(String testString) {
        this.testString = testString;
    }
}
