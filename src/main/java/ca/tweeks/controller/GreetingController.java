package ca.tweeks.controller;

import ca.tweeks.service.GreetingService;
import ca.tweeks.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class GreetingController {

    @Autowired
    private GreetingService greeting;

    @Resource
    private TestService testService;

    @RequestMapping("/greeting")
    @ResponseBody
    public String greeting(@RequestParam(value="name", defaultValue="World") String name) {
        //return new Greeting(counter.incrementAndGet(), String.format(template, name));
        return greeting.showMessage() + testService.showMessage();
    }
}
